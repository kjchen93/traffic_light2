# Traffic Light V2

An embedded traffic light simulator. It is intended to simulate traffic for a street with no cross section and has pedestrians crossing. During the day pedestrians have the option to click on a button in order to switch the pedestrian light to green, whilst turning the traffic light for cars red. During the night however only the traffic light for cars are on, and only blinking yellow light. The images below shows each case.
![](./imgs/traffic_day1.jpg)
![](./imgs/traffic_day2.jpg)
![](./imgs/traffic_night.jpg)

## Author 
[Kai Chen](gitlab.com/kjchen93)