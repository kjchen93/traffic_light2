#include <LiquidCrystal_I2C.h>

#define LDR_PIN 2

// LDR Characteristics
const float GAMMA = 0.7;
const float RL10 = 50;

LiquidCrystal_I2C lcd(0x27, 20, 4);

int red_c = 13;
int yellow_c = 12;
int green_c = 11; 

int red_p = 7;
int green_p = 6; 

bool switched;

void blinkyblinky(){
  digitalWrite(red_p, LOW); 
  digitalWrite(green_p, LOW);
  digitalWrite(red_c, LOW);
  digitalWrite(green_c, LOW);
  delay(1000);
  digitalWrite(yellow_c, HIGH);
  delay(1000);
  digitalWrite(yellow_c, LOW);
}

void daytraffic() {
  int buttonval = digitalRead(1);
  if (buttonval > 0 ){
    if (switched){
      delay(1000);
      digitalWrite(red_c, LOW);
      digitalWrite(yellow_c, HIGH);
      delay(1000);
      switched = false;
    }
    digitalWrite(red_c, LOW);
    digitalWrite(yellow_c, LOW);
    digitalWrite(green_c, HIGH);
    digitalWrite(red_p, HIGH);             
    digitalWrite(green_p, LOW);
    delay(1000); 
    switched = false;
  }
  else{
    if (!switched){
      delay(1000);
      digitalWrite(green_c, LOW);
      digitalWrite(yellow_c, HIGH);
      delay(1000);
      switched = true;
    }
    
    digitalWrite(red_c, HIGH);             
    digitalWrite(yellow_c, LOW);
    digitalWrite(green_c, LOW);
    delay(1000);
    digitalWrite(red_p, LOW); 
    digitalWrite(green_p, HIGH);
    delay(1000);
  }
}

void setup() {
  pinMode(LDR_PIN, INPUT);
  pinMode(1, INPUT_PULLUP);
  lcd.init();
  lcd.backlight();
}

void loop() {
  int analogValue = analogRead(A0);
  float voltage = analogValue / 1024. * 5;
  float resistance = 2000 * voltage / (1 - voltage / 5);
  float lux = pow(RL10 * 1e3 * pow(10, GAMMA) / resistance, (1 / GAMMA));

  lcd.setCursor(2, 0);
  lcd.print("Room: ");
  if (lux > 50) {
    lcd.print("Light!");
    daytraffic();
  } else {
    lcd.print("Dark  ");
    blinkyblinky();
  }

  lcd.setCursor(0, 1);
  lcd.print("Lux: ");
  lcd.print(lux);
  lcd.print("          ");

  delay(100);
  
}
